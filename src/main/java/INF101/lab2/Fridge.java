package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    public int maxCapacity = 20;
    public List<FridgeItem> items;

    public Fridge() {

        items = new ArrayList<FridgeItem>();

        
    }

    public void fridge(FridgeItem putIn){
        



    }

    @Override
    public int nItemsInFridge(){

        return items.size();
    }

    @Override
    public int totalSize(){

        return maxCapacity;

    }

    @Override
    public boolean placeIn(FridgeItem item){
        if(items.size() >= maxCapacity) {
            return false;
        }

        items.add(item); 
        return true;
        
    }

    @Override
    public void takeOut(FridgeItem item){


        if(!items.contains(item)) {
            throw new NoSuchElementException();
        }

        items.remove(item);

     }

    @Override
    public void emptyFridge() {

        items.clear();

    }

    @Override
    public List<FridgeItem> removeExpiredFood(){

        List<FridgeItem> expiredFood = new ArrayList<FridgeItem>();
        for(FridgeItem item: items) {
            if (item.hasExpired()){
                expiredFood.add(item);
                
            }
        }
            
            for (FridgeItem item : expiredFood) {
                items.remove(item);

            }
        
    


        return expiredFood;
        
    }

    
}
